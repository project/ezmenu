The goal of this module is to prevent users from having to worry about path attributes,
and unnecessarily repetitive menu fieldsets on node edit forms.  With the excellent
new menu drag-and-drop tool in Drupal 6, users should never have to worry about parents
and weights, and now they won't have to worry about menu titles or paths either.

The menu works fine, but it is still in development because it could use a little
sprucing-up as far as administrative settings go.

The module requires no configuration.  Just install it as you normally would, and you will
notice a couple of changes on the site:

- Node edit forms no longer have a menu fieldset
- On each menu's "List items" page, you will see links for "Add a page to this menu".
-- When a user clicks this link, she will be taken to the node/add/page form.
-- After saving the page, the page will be added to the top of the menu from which she had clicked "Add a page to this menu".
-- The user will be redirected to the original "List items" page, and prompted to place the page in the appropriate location.

There is also a very simple API, mainly for installation profile developers, to make it easy
for users to add views to a menu:

/**
 * Provide a list of views which the user may add to a menu via the ezmenu interface
 * @return An array of the form array('path1' => t('Name of view 1'), ..., 'pathN' => t('Name of view N'))
 * where each key is an internal Drupal path, and each value is the descriptive name for
 * that path to be displayed in the ezmenu views interface.
 */
function hook_ezmenu_views_links();

When one or more modules implements hook_ezmenu_views_links(), ezmenu will display a link
on the "List items" form titled "Add a dynamically-generated page to this menu".  When the user
clicks the link, she will be taken to a page which lists all of the links returned by
hook_ezmenu_views_links as radio buttons.  She selects one of those radio buttons and clicks
"save", and the menu item - including a title drawn from the view, the path specified in the
hook_ezmenu_views_links implementation - will be added to the top of the menu.  She will
then be prompted to drag the menu item into the appropriate location on the menu.

An example implementation is:

function mymodule_ezmenu_views_links() {
  return array(
    'calendar' => t('Monthly calendar display'),
    'books/fiction' => t('List of fiction books'),
    'books/nonfiction' => t('List of non-fiction books'),
  );
}

Ultimately, the goal of this module is to allow users to manage all of their day-to-day content
management needs by starting with a look at their menu.  That paradigm is, I think, very
appropriate for a mid-sized site with a lot of different sections arranged into a carefully-
designed site map.

There are a lot of things that need to happen before that becomes a realistic approach, though:
- Add "edit page" and "delete page" to each row of the menu table, and help users distinguish
between those links and "edit item"/"delete item".
- Hide "enabled" and "expanded", and replace with sensible defaults and/or action-oriented
links, e.g. "hide menu item".
- Allow users to expand and collapse sections of the site map when viewing the List items page.
- Allow users to add a page or a view beneath any parent item in the map, rather than only at
the root.
- Integrate with the viewreference module, allowing users to - in one step - create a page
which embeds a view, and adds that page to the menu.
- Allow administrators to specify which content types may be added directly from the List items
page, rather than hard-coding page.  Perhaps piggy-back on the settings provided by the ctm module.
- Add code which is a bit more savvy about generating views links, e.g., automatically listing the paths
to all views which are enabled, have a page display, and have no arguments.
-- Allow other modules to alter the ezmenu_views_links array before rendering.

This is my to-do list for the time being, but I'm certainly open to other suggestions!